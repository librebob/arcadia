#include "screenshot.h"

Screenshot::Screenshot(QObject *parent) : QObject(parent)
{

}

Screenshot::Screenshot(QUrl thumb_url, QUrl source_url, QObject *parent)
    : QObject(parent)
    , m_thumb_url(thumb_url)
    , m_source_url(source_url)
{

}

QUrl Screenshot::thumbUrl()
{
    return m_thumb_url;
}

QUrl Screenshot::sourceUrl()
{
    return m_source_url;
}
