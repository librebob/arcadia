#include "release.h"

Release::Release(QObject *parent) : QObject(parent)
{
}

Release::Release(QDateTime timestamp, QString version, QString description, QObject *parent)
    : QObject(parent)
    , m_timestamp(timestamp)
    , m_version(version)
    , m_description(description)
{

}

qint64 Release::timestamp()
{
    return m_timestamp.toSecsSinceEpoch();
}

QString Release::version()
{
    return m_version;
}

QString Release::description()
{
    return m_description;
}
