import QtQuick 2.9
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.15
import Athenaeum 1.0

TextField {
//    color: Material.background

    Timer {
        id: timer
        interval: 500
        //running: true; repeat: true
        running: false
        repeat: false
        onTriggered: {
            console.log("Triggering search")
            library.searchValue = text
        }
    }

    placeholderText: qsTr('Search %L1 Games...').arg(library.games.length)

    onTextChanged: {
        if (text === '') {
            timer.stop()
            library.searchValue = text
            return
        }

        if (timer.running) {
             console.log("Triggering timer reset")
            timer.restart()
        } else {
             console.log("Triggering timer start")
            timer.start()
        }

//        library.searchValue = text
    }
    Keys.onEscapePressed: {
        text = ''
    }

    Button {
        id: clearText
        anchors {
            top: parent.top
            right: parent.right
            bottom: parent.bottom
        }

        background: Rectangle {
            anchors.fill: parent
            color: tr
        }

        visible: searchField.text
        icon.source: 'icons/close.svg'
        icon.height: 15
        icon.width: 15

        onClicked: {
            searchField.text = ""
            searchField.forceActiveFocus()
        }
    }
}
