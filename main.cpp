#include <iostream>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QQmlContext>
#include <QQuickStyle>
#include <QNetworkAccessManager>
#include <QtConcurrent>

#include "settings.h"
#include "library.h"
#include "loader.h"
#include "sources/app.h"
#include "network/cachingnetworkaccessmanagerfactory.h"


int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    QQuickStyle::setStyle("Material");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    CachingNetworkAccessManagerFactory* nam_factory = new CachingNetworkAccessManagerFactory();
    engine.setNetworkAccessManagerFactory(nam_factory);

//    qRegisterMetaType<QList<App*>>("QList<App*>");
    Loader* loader = new Loader();

    Settings* settings = new Settings;
    Library* library = new Library;

    QObject::connect(loader, &Loader::loaded, library, &Library::setGames);

//    QtConcurrent::run(loader, &Loader::load);
    loader->load();

    qmlRegisterType<Screenshot>("Athenaeum", 1, 0, "Screenshot");
    qmlRegisterType<Url>("Athenaeum", 1, 0, "Url");
    qmlRegisterType<Release>("Athenaeum", 1, 0, "Release");
    qmlRegisterType<Game>("Athenaeum", 1, 0, "Game");

//    qmlRegisterType<GameManager>("Athenaeum", 1, 0, "GameManager");
    engine.rootContext()->setContextProperty(QString("loader"), loader);
    engine.rootContext()->setContextProperty(QString("settings"), settings);
    engine.rootContext()->setContextProperty(QString("library"), library);

    engine.load(url);

    return app.exec();
}
