import QtQuick 2.9
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.15
import Athenaeum 1.0

Button {
    flat: true
    icon.source: 'icons/filter.svg'
    width: 30
    highlighted: tagGrid.activeTags.length
    onClicked: {
        filterPopup.opened ? filterPopup.close() : filterPopup.open()
    }
    Popup {
        id: filterPopup
        visible: false
        x: parent.width
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

        Column {
            anchors.fill: parent
            Grid {
                id: tagGrid
                property var activeTags: []
                columns: 3
                spacing: 0

                ButtonGroup {
                    id: tagGroup
                    buttons: tagRepeater.children
                    exclusive: false
                }

                Repeater {
                    id: tagRepeater
                    model: [
                        { id: 'Action', text: qsTr('Action') },
                        { id: 'Adventure', text: qsTr('Adventure') },
                        { id: 'Arcade', text: qsTr('Arcade') },
                        { id: 'Board', text: qsTr('Board') },
                        { id: 'Blocks', text: qsTr('Blocks') },
                        { id: 'Card', text: qsTr('Card') },
                        { id: 'Kids', text: qsTr('Kids') },
                        { id: 'Logic', text: qsTr('Logic') },
                        { id: 'RolePlaying', text: qsTr('RolePlaying') },
                        { id: 'Shooter', text: qsTr('Shooter') },
                        { id: 'Simulation', text: qsTr('Simulation') },
                        { id: 'Sports', text: qsTr('Sports') },
                        { id: 'Strategy', text: qsTr('Strategy') }
                    ]
                    delegate: CheckBox {
                        text: tagRepeater.model[index].text
                        ButtonGroup.group: tagGroup
                        onCheckedChanged: {
                            tagGrid.activeTags = tagGrid.activeTags.filter(tag => tag !== tagRepeater.model[index].id)
                            if (checked) {
                                tagGrid.activeTags = tagGrid.activeTags.concat(tagRepeater.model[index].id)
                            }
                        }
                    }
                }
            }
            RowLayout {
                width: parent.width
                Button {
                    text: qsTr('Apply')
                    Layout.fillWidth: true
                    onClicked: {
                        library.tagsValue = tagGrid.activeTags
                    }
                }
                Button {
                    enabled: tagGrid.activeTags.length
                    Layout.fillWidth: true
                    text: qsTr('Clear')
                    onClicked: {
                        tagGroup.checkState = Qt.Unchecked
                        tagGrid.activeTags = []
                        library.tagsValue = tagGrid.activeTags
                    }
                }
            }
        }
    }
}
