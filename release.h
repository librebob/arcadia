#ifndef RELEASE_H
#define RELEASE_H

#include <QObject>
#include <QDateTime>

class Release : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qint64 timestamp READ timestamp NOTIFY releaseChanged)
    Q_PROPERTY(QString version READ version NOTIFY releaseChanged)
    Q_PROPERTY(QString description READ description NOTIFY releaseChanged)
public:
    explicit Release(QObject *parent = nullptr);
    Release(QDateTime, QString, QString, QObject *parent = nullptr);
    qint64 timestamp();
    QString version();
    QString description();

private:
    QDateTime m_timestamp;
    QString m_version;
    QString m_description;

Q_SIGNALS:
    void releaseChanged();
};

#endif // RELEASE_H
