#include "library.h"

Library::Library(QObject *parent) : QObject(parent)
{
    connect(this, &Library::gamesChanged, this, &Library::updateFilters);
}

void Library::setGames(QList<Game*> games)
{
    std::cout << "here" << std::endl;
    m_games = games;
    Q_EMIT gamesChanged();
}

int Library::getCount(QString filter_set)
{
    return m_filters[filter_set].count();
}

void Library::updateFilters()
{
    m_filters["all"] = m_games;
    m_filters["installed"] = QtConcurrent::blockingFiltered(m_games, [](Game* game) {
        return game->installed();
    });
    m_filters["recent"] = m_games;
    m_filters["has_update"] = QtConcurrent::blockingFiltered(m_games, [](Game* game) {
        return game->hasUpdate();
    });

    m_filters["processing"] = QtConcurrent::blockingFiltered(m_games, [](Game* game) {
        return game->processing();
    });

    Q_EMIT filterKeyChanged();
}

QQmlListProperty<Game> Library::games()
{
    if (m_search_value.length()) {
        m_filters["search"] = QList<Game*>();
        for (Game* game : m_filters[m_filter_key]) {
            if (game->name().contains(m_search_value, Qt::CaseInsensitive)) {
                m_filters["search"].append(game);
            }
        }
        return QQmlListProperty<Game>(this, &m_filters["search"]);
    }

    return QQmlListProperty<Game>(this, &m_filters[m_filter_key]);
}
