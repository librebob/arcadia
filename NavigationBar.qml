import QtQuick 2.9
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.3
import Athenaeum 1.0
ToolBar {
    id: toolBar
    property string activeView
    Rectangle {
        anchors.fill: parent
        color: Material.background
    }

    RowLayout {
        ToolButton {
            highlighted: activeView === 'library'
            text: qsTr('Library')
            icon.source: 'icons/library.svg'
            onClicked: {
                if (!highlighted){
                    enter(libraryView, null)
                }
            }
        }
        ToolSeparator {}
        ToolButton {
            highlighted: activeView === 'servers'
            text: qsTr('Servers')
            icon.source: 'icons/servers.svg'
            onClicked: {
                if (!highlighted) {
                    enter(serversView, null)
                }
            }
        }
        ToolSeparator {}
        ToolButton {
            highlighted: activeView === 'settings'
            text: qsTr('Settings')
            icon.source: 'icons/settings.svg'
            onClicked: {
                if (!highlighted) {
                    enter(settingsView, null)
                }
            }
        }
    }
    RowLayout {
        visible: activeView === 'library'
        anchors.right: menuButton.left
        ToolButton {
            id: listButton
            enabled: library.view !== 'list'
            icon.source: 'icons/list.svg'
            onClicked: {
                libraryParent.viewMode = 'list'
                // enabled = false
            }
            ToolTip.visible: hovered
            ToolTip.text: qsTr("Show games in a list view.")
        }
        ToolButton {
            id: gridButton
            enabled: library.view !== 'grid'
            icon.source: 'icons/grid.svg'
            onClicked: {
                libraryParent.viewMode = 'grid'
            }
            ToolTip.visible: hovered
            ToolTip.text: qsTr("Show games in a grid view.")
        }
    }
    RowLayout {
        visible: activeView === 'settings'
        anchors.right: menuButton.left
        ToolButton {
            icon.source: 'icons/reset.svg'
            ToolTip.visible: hovered
            ToolTip.text: qsTr("Reset settings to defaults.")
        }
    }

    ToolButton {
        id: menuButton
        anchors.right: parent.right
        icon.source: 'icons/menu.svg'
        onClicked: menu.open()
        Menu {
            id: menu
            MenuItem {
                text: qsTr('Check For Updates')
                onTriggered: window.checkAll()
            }
            MenuItem {
                text: qsTr('Update All')
                onTriggered: window.updateAll()
            }
            MenuItem {
                text: qsTr('Exit')
                onTriggered: library.processingCount > 0 ? confirmExit.open() : Qt.quit()
                Popup {
                    id: confirmExit
                    x: Math.round((parent.width - width) / 2)
                    y: Math.round((parent.height - height) / 2)
                    parent: stackView
                    dim: true
                    modal: true
                    contentItem: Column {
                        spacing: 20
                        Text {
                            anchors.horizontalCenter: parent.horizontalCenter
                            color: Material.foreground
                            font.pixelSize: 20
                            text: qsTr('You have operations pending.')
                        }
                        Row {
                            spacing: 20
                            anchors.horizontalCenter: parent.horizontalCenter
                            Button {
                                text: qsTr('Close Anyway')
                                onClicked: {
                                    Qt.quit()
                                }
                            }
                            Button {
                                text: qsTr('Cancel')
                                onClicked: {
                                    confirmExit.close()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
