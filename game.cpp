#include "game.h"

Game::Game(QObject *parent) : QObject(parent)
{
//    Screenshot* screenshot = new Screenshot();
//    m_screenshots.append(screenshot);

//    Url* url = new Url();
//    m_urls.append(url);

//    Release* release = new Release();
//    m_releases.append(release);
}

Game::Game(QList<App*> apps, QObject *parent)
    : QObject(parent)
    , m_apps(apps)
{
    for (App* app : m_apps) {
        connect(app, &App::stateChanged, this, &Game::stateChanged);
    }
    m_app = apps.first();
}

QString Game::id()
{
    std::cout << m_app->id().toStdString() << std::endl;
    return m_app->id();
}

QStringList Game::sources()
{
    QStringList sourceNames;
    for (App* app : m_apps) {
        sourceNames.append(app->sourceName());
    }

    return sourceNames;
}

QString Game::source()
{
    std::cout << m_app->sourceName().toStdString() << std::endl;
    return m_app->sourceName();
}

void Game::setSource(QString source)
{
    for (App* app : m_apps) {
        if (app->sourceName() == source) {
            m_app = app;
            Q_EMIT dataChanged();
            Q_EMIT stateChanged();
            return;
        }
    }
}

QString Game::name()
{
    return m_app->m_component.name();
}

QUrl Game::iconSmall()
{
    return m_app->m_component.icon(QSize(64, 64)).url();
}

QUrl Game::iconLarge()
{
    QUrl icon = m_app->m_component.icon(QSize(128, 128)).url();
    if (icon.isEmpty()) {
        return iconSmall();
    }

    return icon;
}

QString Game::license()
{
    return m_app->m_component.projectLicense();
}

QString Game::summary()
{
    return m_app->m_component.summary();
}

QString Game::description()
{
    return m_app->m_component.description();
}

QString Game::developerName()
{
    QString name = m_app->m_component.developerName();

    if (name.isEmpty()) {
        name = m_app->m_component.projectGroup();
    }

    return name;
}

QList<QString> Game::categories()
{
    return m_app->m_component.categories();
}

QList<QString> Game::antiFeatures()
{
    return m_anti_features;
}

QQmlListProperty<Screenshot> Game::screenshots()
{
//    if (!m_screenshots.length() && m_component.screenshots().length()) {

//    }
    // TODO: fix memory leak
    m_screenshots.clear();
    for(AppStream::Screenshot screenshot : m_app->m_component.screenshots()) {
        AppStream::Image thumb;
        AppStream::Image source;
        for(AppStream::Image image : screenshot.images()) {
            if (image.kind() == AppStream::Image::KindSource) {
                source = image;
            }
            if (image.kind() == AppStream::Image::KindThumbnail) {
                thumb = image;
            }
        }
        m_screenshots.append(new Screenshot(thumb.url(), source.url()));
    }
    return QQmlListProperty<Screenshot>(this, &m_screenshots);
}

QQmlListProperty<Url> Game::urls()
{
    QList<QString> types = {
        "homepage","bugtracker","help",
        "faq","donation","translate",
        "unknown","manifest","contact"
    };

    m_urls.clear();

    for (QString type : types) {
        QUrl url = m_app->m_component.url(m_app->m_component.stringToUrlKind(type));
        if (url.isEmpty()) {
            continue;
        }

        m_urls.append(new Url(url, type));
    }

    return QQmlListProperty<Url>(this, &m_urls);
}

QQmlListProperty<Release> Game::releases()
{
//    if (!m_releases.length() && m_component.releases().length()) {

//    }
    // TODO: fix memory leak
    m_releases.clear();
    for(AppStream::Release release : m_app->m_component.releases()) {
        m_releases.append(new Release(release.timestamp(), release.version(), release.description()));
    }
    return QQmlListProperty<Release>(this, &m_releases);
}

bool Game::installed()
{
    return m_app->m_installed;
}

bool Game::hasUpdate()
{
    return m_app->m_has_update;
}

bool Game::processing()
{
    return m_app->m_processing;
}

bool Game::playing()
{
    return m_app->m_playing;
}

void Game::play()
{
    m_app->play();
}

void Game::install()
{
    m_app->install();
}

void Game::uninstall()
{
    m_app->uninstall();
}
