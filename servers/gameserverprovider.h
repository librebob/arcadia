#ifndef GAMESERVERPROVIDER_H
#define GAMESERVERPROVIDER_H

#include <QObject>

class GameServerProvider : public QObject
{
    Q_OBJECT
public:
    explicit GameServerProvider(QObject *parent = nullptr);
    virtual void fetchServers() = 0;

Q_SIGNALS:

};

#endif // GAMESERVERPROVIDER_H
