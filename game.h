#ifndef GAME_H
#define GAME_H

#include <QtCore>
#include <QtConcurrent>
#include <QObject>
#include <QQmlListProperty>
#include "screenshot.h"
#include "release.h"
#include "url.h"
#include "sources/flatpak-helper.h"
#include "sources/app.h"
#include <iostream>
#include <AppStreamQt/component.h>
#include <AppStreamQt/screenshot.h>
#include <AppStreamQt/image.h>
#include <AppStreamQt/release.h>
#include <AppStreamQt/icon.h>

class Game : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString id READ id NOTIFY dataChanged)
    Q_PROPERTY(QString name READ name NOTIFY dataChanged)
    Q_PROPERTY(QUrl iconSmall READ iconSmall NOTIFY dataChanged)
    Q_PROPERTY(QUrl iconLarge READ iconLarge NOTIFY dataChanged)
    Q_PROPERTY(QString license READ license NOTIFY dataChanged)
    Q_PROPERTY(QString summary READ summary NOTIFY dataChanged)
    Q_PROPERTY(QString description READ description NOTIFY dataChanged)
    Q_PROPERTY(QString developerName READ developerName NOTIFY dataChanged)
    Q_PROPERTY(QList<QString> categories READ categories NOTIFY dataChanged)
    Q_PROPERTY(QQmlListProperty<Screenshot> screenshots READ screenshots NOTIFY dataChanged)
    Q_PROPERTY(QQmlListProperty<Url> urls READ urls NOTIFY dataChanged)
    Q_PROPERTY(QQmlListProperty<Release> releases READ releases NOTIFY dataChanged)
    Q_PROPERTY(QList<QString> antiFeatures READ antiFeatures NOTIFY dataChanged)
    Q_PROPERTY(bool installed READ installed NOTIFY stateChanged)
    Q_PROPERTY(bool hasUpdate READ hasUpdate NOTIFY stateChanged)
    Q_PROPERTY(bool processing READ processing NOTIFY stateChanged)
    Q_PROPERTY(bool playing READ playing NOTIFY stateChanged)
    Q_PROPERTY(QStringList sources READ sources NOTIFY stateChanged)
    Q_PROPERTY(QString source READ source WRITE setSource NOTIFY dataChanged)
public:
    explicit Game(QObject *parent = nullptr);
//    Game(AppStream::Component, FlatpakInstallation*, FlatpakRemote*, QObject *parent = nullptr);
    Game(QList<App*> apps, QObject *parent = nullptr);
    QString id();
    QString arch();
    QString branch();
    QString name();
    QUrl iconSmall();
    QUrl iconLarge();
    QString license();
    QString summary();
    QString description();
    QString developerName();
    QList<QString> categories();
    QQmlListProperty<Screenshot> screenshots();
    QQmlListProperty<Url> urls();
    QQmlListProperty<Release> releases();
    QList<QString> antiFeatures();
    bool installed();
    bool hasUpdate();
    bool processing();
    bool playing();
    QStringList sources();
    QString source();


public Q_SLOTS:
    void play();
    void install();
    void uninstall();
    void setSource(QString);

private:
    QList<App*> m_apps;
    App* m_app;

    QList<Screenshot*> m_screenshots;
    QList<Url*> m_urls;
    QList<Release*> m_releases;
    QList<QString> m_anti_features;


Q_SIGNALS:
    void dataChanged();
    void stateChanged();

};

#endif // GAME_H
