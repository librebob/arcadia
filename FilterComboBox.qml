import QtQuick 2.9
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.15
import Athenaeum 1.0

ComboBox {
    currentIndex: getFilterIndex(library.filterKey)

    property string filterIndex: library.filterKey
    onFilterIndexChanged: {
        currentIndex = getFilterIndex(library.filterKey)
    }
    onModelChanged: {
        currentIndex = getFilterIndex(library.filterKey)
    }
    onActivated: {
        library.filterKey = getFilterKey(index)
        searchField.text = ''
    }
    function getFilterIndex(key) {
        switch(key) {
            case 'all':
                return 0;
            case 'installed':
                return 1;
            case 'recent':
                return 2;
            case 'has_update':
                return 3;
            case 'processing':
                return 4;
        }
    }
    function getFilterKey(index) {
        switch(index) {
            case 0:
                return 'all';
            case 1:
                return 'installed';
            case 2:
                return 'recent';
            case 3:
                return 'has_update';
            case 4:
                return 'processing';
        }
    }

    model: [
        qsTr('All (%L1)').arg(library.getCount('all')),
        qsTr('Installed (%L1)').arg(library.getCount('installed')),
        qsTr('Recent (%L1)').arg(library.getCount('recent')),
        qsTr('Has Updates (%L1)').arg(library.getCount('has_update')),
        qsTr('Processing (%L1)').arg(library.getCount('processing'))
    ]
    validator: IntValidator {
        top: 5
        bottom: 0
    }
}
