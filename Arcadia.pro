QT += quick dbus network xml quickcontrols2 concurrent

CONFIG += c++11 no_keywords

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
LIBS += -lAppStreamQt
LIBS += -lpackagekitqt5
INCLUDEPATH += "/usr/include/packagekitqt5/PackageKit"
#LIBS += -lAppStreamQt
#INCLUDEPATH += "/usr/include/AppStreamQt"

SOURCES += \
        game.cpp \
        icon.cpp \
        library.cpp \
        loader.cpp \
        main.cpp \
        network/cachingnetworkaccessmanager.cpp \
        network/cachingnetworkaccessmanagerfactory.cpp \
        release.cpp \
        screenshot.cpp \
        servers/gameserver.cpp \
        servers/gameserverprovider.cpp \
        servers/serveraggreator.cpp \
        settings.cpp \
        sources/app.cpp \
        sources/appsource.cpp \
        sources/flatpakapp.cpp \
        sources/flatpakappsource.cpp \
        sources/packagekitapp.cpp \
        sources/packagekitappsource.cpp \
        url.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#LIBS += -L/usr/local/lib64 -lAppStreamQt
#INCLUDEPATH += "/usr/local/include/AppStreamQt"

HEADERS += \
    game.h \
    icon.h \
    library.h \
    lists.h \
    loader.h \
    network/cachingnetworkaccessmanager.h \
    network/cachingnetworkaccessmanagerfactory.h \
    release.h \
    screenshot.h \
    servers/gameserver.h \
    servers/gameserverprovider.h \
    servers/serveraggreator.h \
    settings.h \
    sources/app.h \
    sources/appsource.h \
    sources/flatpak-helper.h \
    sources/flatpakapp.h \
    sources/flatpakappsource.h \
    sources/packagekitapp.h \
    sources/packagekitappsource.h \
    url.h


unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += glib-2.0
unix: PKGCONFIG += flatpak

DISTFILES += \
      icons/CREDITS \
      icons/LICENSE \
      icons/add.svg \
      icons/browse.svg \
      icons/bug.svg \
      icons/close.svg \
      icons/cogs.svg \
      icons/contact.svg \
      icons/donate.svg \
      icons/download.svg \
      icons/exclamation.svg \
      icons/filter.svg \
      icons/globe.svg \
      icons/grid.svg \
      icons/help.svg \
      icons/home.svg \
      icons/left.svg \
      icons/library.svg \
      icons/link.svg \
      icons/list.svg \
      icons/manifest.svg \
      icons/menu.svg \
      icons/play.svg \
      icons/question.svg \
      icons/refresh.svg \
      icons/reset.svg \
      icons/right.svg \
      icons/search.svg \
      icons/servers.svg \
      icons/settings.svg \
      icons/thumbs-down.svg \
      icons/thumbs-up.svg \
      icons/trash.svg
