#include "url.h"

Url::Url(QObject *parent) : QObject(parent)
{

}

Url::Url(QUrl url, QString type, QObject *parent)
    : QObject(parent)
    , m_url(url)
    , m_type(type)
{
}

QUrl Url::url()
{
    return m_url;
}

QString Url::type()
{
    return m_type;
}

QString Url::symbol()
{
    if ("homepage" == type())
        return "home.svg";
    else if ("bugtracker" == type())
        return "bug.svg";
    else if ("help" == type())
        return "help.svg";
    else if ("faq" == type())
        return "question.svg";
    else if ("donation" == type())
        return "donate.svg";
    else if ("translate" == type())
        return "globe.svg";
    else if ("manifest" == type())
        return "manifest.svg";
    else if ("contact" == type())
        return "contact.svg";
    return "cogs.svg";
}
