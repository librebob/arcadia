#ifndef LOADER_H
#define LOADER_H

#include <QtCore>
#include <QObject>
#include <QList>
#include "sources/flatpakappsource.h"
#include "sources/packagekitappsource.h"
#include "game.h"

class Loader : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool isLoading READ isLoading NOTIFY loading)

public:
    explicit Loader(QObject *parent = nullptr);

    bool isLoading();

public Q_SLOTS:
    void load();

private:
    FlatpakAppSource m_flatpak_app_source;
    PackageKitAppSource m_package_kit_app_source;
    QList<Game *> m_games;
    QMultiMap<QString, App*> apps_map;
    bool m_loading = true;
    void processApps(QList<App*>);
    int m_wait_for = 2;

Q_SIGNALS:
    void process();
    void loaded(QList<Game*>);
    void loading();
};

#endif // LOADER_H
