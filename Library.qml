import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.3
import Athenaeum 1.0

Page {
    id: libraryParent
    Rectangle {
        anchors.fill: parent
        color: Material.background
    }
    header: NavigationBar {
        activeView: 'library'
    }
    property string viewMode: 'list'
    LibraryList {
        visible: viewMode === 'list'
    }
    LibraryGrid {
        visible: viewMode === 'grid'
    }
}
