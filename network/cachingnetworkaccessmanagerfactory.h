#ifndef CACHINGNETWORKACCESSMANAGERFACTORY_H
#define CACHINGNETWORKACCESSMANAGERFACTORY_H

#include <QQmlNetworkAccessManagerFactory>
#include "cachingnetworkaccessmanager.h"

class CachingNetworkAccessManagerFactory : public QQmlNetworkAccessManagerFactory
{
public:
    CachingNetworkAccessManagerFactory();
    QNetworkAccessManager *create(QObject *parent);
};

#endif // CACHINGNETWORKACCESSMANAGERFACTORY_H
