#include "cachingnetworkaccessmanagerfactory.h"

CachingNetworkAccessManagerFactory::CachingNetworkAccessManagerFactory()
{

}


QNetworkAccessManager* CachingNetworkAccessManagerFactory:: create(QObject *parent)
{
    return new CachingNetworkAccessManager(parent);
}
