#include "cachingnetworkaccessmanager.h"

CachingNetworkAccessManager::CachingNetworkAccessManager(QObject *parent) : QNetworkAccessManager(parent)
{

}

QNetworkReply* CachingNetworkAccessManager::get(const QNetworkRequest &request)
{
    QNetworkRequest nonConstRequest = request;
    nonConstRequest.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::PreferCache);
    return QNetworkAccessManager::get(request);
}
