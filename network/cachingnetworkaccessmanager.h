#ifndef CACHINGNETWORKACCESSMANAGER_H
#define CACHINGNETWORKACCESSMANAGER_H

#include <QNetworkAccessManager>

class CachingNetworkAccessManager : public QNetworkAccessManager
{
public:
    explicit CachingNetworkAccessManager(QObject *parent = nullptr);
    QNetworkReply* get(const QNetworkRequest &request);
};

#endif // CACHINGNETWORKACCESSMANAGER_H
