import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.3
import Athenaeum 1.0

ApplicationWindow {
    width: 1280
    height: 720
    visible: true
    title: qsTr("Arcadia")

    Component.onCompleted: {
//        loader.load()
    }

    property int theme: Material.Dark
    property color tr: 'transparent'
    Material.theme: Material.Dark
    Material.accent: Material.LightBlue
    Material.primary: Material.Grey

    property var stack: []
    property int stackIndex: 0

    property int libraryView: 0
    property int serversView: 1
    property int settingsView: 2

    function makeRequest (opts) {
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest()
            xhr.open(opts.method, opts.url)
            xhr.onload = function () {
                if (xhr.status >= 200 && xhr.status < 300) {
                    resolve({
                        response: xhr.response,
                        params: opts.params
                    })
                } else {
                    reject({
                        status: xhr.status,
                        statusText: xhr.statusText
                    });
                }
            }
            xhr.onerror = function () {
                reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                })
            }

            xhr.setRequestHeader("Content-type", "application/json")
            xhr.send(JSON.stringify(opts.params))
        })
    }

    function changeView(view, details) {
        if (details) {
            switch (view) {
                case libraryView:
                    library.updateCurrentGame(details)
                    break
                case serversView:
                    serverProvider.filterGame = details
                    if (!serverProvider.servers.length) {
                        serverProvider.refresh()
                    }
                    break
            }
        }

        stackView.currentIndex = view
    }

    function enter(view, details) {
        stack.splice(stackIndex + 1, stack.length - stackIndex - 1, [view, details])
        stackIndex = stack.length - 1

        changeView(view, details)
    }

    StackLayout {
        id: stackView
        anchors.fill: parent
        visible: !loader.isLoading
        Component.onCompleted: {
            enter(libraryView, null)
        }

        Library {}
        Servers {}
        Settings {}
    }

    Rectangle {
        anchors.fill: parent
        color: Material.background
        visible: loader.isLoading

        BusyIndicator {
            id: loadingIndicator
            anchors.centerIn: parent
            running: loader.isLoading// && !loader.error
        }

        Text {
            id: loadingMessage
            anchors.top: loadingIndicator.bottom
            text: 'loader.message'
            color: Material.foreground
            width: parent.width
            topPadding: 10
            horizontalAlignment: Text.AlignHCenter
        }

        /* Logs */
        Rectangle {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.rightMargin: 40
            anchors.leftMargin: 40
            anchors.bottom: parent.bottom
            color: Material.background
            height: 160

            Flickable {
                id: testFlick
                anchors.fill: parent

                clip: true
                boundsBehavior: Flickable.StopAtBounds

                TextArea {
                    id: ta
                    onContentHeightChanged: {
                        testFlick.contentY = (contentHeight <= 150 ? 0 : contentHeight - 150)
                    }
                    color: Material.foreground
                    readOnly: true
                    text: 'loader.log'
                    background: Rectangle {
                        anchors.fill: parent
                        color: Material.background
                    }
                }
            }
        }
    }
}
