#ifndef LIBRARY_H
#define LIBRARY_H

#include <iostream>
#include <QObject>
#include <QQmlListProperty>
#include "game.h"

class Library : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<Game> games READ games NOTIFY filterKeyChanged)
    Q_PROPERTY(QString filterKey MEMBER m_filter_key NOTIFY filterKeyChanged)
    Q_PROPERTY(Game* selectedGame MEMBER m_selected_game NOTIFY selectedGameChanged)
    Q_PROPERTY(QString searchValue MEMBER m_search_value NOTIFY filterKeyChanged)
public:
    explicit Library(QObject *parent = nullptr);
    QQmlListProperty<Game> games();


public Q_SLOTS:
    void setGames(QList<Game*>);
    int getCount(QString);

private:
    void updateFilters();
    QList<Game *> m_games;
    QString m_filter_key = "all";
    QMap<QString, QList<Game*>> m_filters;
    Game* m_selected_game = nullptr;
    QString m_search_value = "";

Q_SIGNALS:
    void gamesChanged();
    void filterKeyChanged();
    void selectedGameChanged();
};

#endif // LIBRARY_H
