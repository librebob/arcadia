#ifndef URL_H
#define URL_H

#include <QObject>
#include <QUrl>

class Url : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QUrl url READ url NOTIFY urlChanged)
    Q_PROPERTY(QString type READ type NOTIFY urlChanged)
    Q_PROPERTY(QString symbol READ symbol NOTIFY urlChanged)
private:
    QUrl m_url;
    QString m_type;

public:
    explicit Url(QObject *parent = nullptr);
    Url(QUrl, QString, QObject *parent = nullptr);
    QUrl url();
    QString type();
    QString symbol();

Q_SIGNALS:
    void urlChanged();
};

#endif // URL_H
