#ifndef SCREENSHOT_H
#define SCREENSHOT_H

#include <QObject>
#include <QUrl>

class Screenshot : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QUrl thumbUrl READ thumbUrl NOTIFY screenshotChanged)
    Q_PROPERTY(QUrl sourceUrl READ sourceUrl NOTIFY screenshotChanged)
public:
    explicit Screenshot(QObject *parent = nullptr);
    Screenshot(QUrl, QUrl, QObject *parent = nullptr);
    QUrl thumbUrl();
    QUrl sourceUrl();

private:
    QUrl m_thumb_url;
    QUrl m_source_url;

Q_SIGNALS:
    void screenshotChanged();
};
#endif // SCREENSHOT_H
