#include "app.h"

//App::App()
//{

//}

void App::startProcessing()
{
    m_processing = true;
    Q_EMIT stateChanged();
}

void App::stopProcessing()
{
    m_processing = false;
    Q_EMIT stateChanged();
}

void App::startPlaying()
{
    m_playing = true;
    Q_EMIT stateChanged();
}

void App::stopPlaying()
{
    m_playing = false;
    Q_EMIT stateChanged();
}
