#include "appsource.h"

AppSource::AppSource(QObject *parent) : QObject(parent)
{

}

bool AppSource::isAcceptableGame(AppStream::Component* component)
{
    if (!component->categories().contains(QString("Game"))) {
        return false;
    }

    if (
        component->projectLicense().startsWith(QString("LicenseRef-Proprietary")) ||
        component->projectLicense().startsWith(QString("LicenseRef-proprietary")) ||
        component->projectLicense().startsWith(QString("CC-BY-NC"))
    ) {
        return false;
    }

    if (
        component->categories().contains(QString("Emulator")) ||
        component->categories().contains(QString("PackageManager")) ||
        component->categories().contains(QString("System")) ||
        component->categories().contains(QString("Utility"))
    ){
        return false;
    }

    return true;
}
