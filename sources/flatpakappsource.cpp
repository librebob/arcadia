#include "flatpakappsource.h"
#include <glib.h>

FlatpakAppSource::FlatpakAppSource(QObject *parent) : m_cancellable(g_cancellable_new())
{
    pool = new AppStream::Pool;
    loadInstallations();
}

bool FlatpakAppSource::loadInstallations()
{
    g_autoptr(GError) error = nullptr;
//    auto installations = flatpak_get_system_installations(m_cancellable, &error);
//    if (error) {
//        std::cout << "Failed to call flatpak_get_system_installations:" << error->message << std::endl;
//    }

//    for (uint i = 0; installations && i < installations->len; i++) {
//        auto installation = FLATPAK_INSTALLATION(g_ptr_array_index(installations, i));
//        g_object_ref(installation);
//        m_installations << installation;
//    }

    auto user = flatpak_installation_new_user(m_cancellable, &error);
    if (user) {
        m_installations << user;
    }

    return !m_installations.isEmpty();
}

void FlatpakAppSource::loadGamesFromAppstreamData()
{

    g_autoptr(GError) error = nullptr;
    QList<App*> apps;
    QMap<QString, bool> installed_map;
    QMap<QString, bool> has_update_map;

    for (FlatpakInstallation* flatpakInstallation : m_installations) {
        g_autoptr(GPtrArray) installed = flatpak_installation_list_installed_refs_by_kind(flatpakInstallation,
                                                                                          FlatpakRefKind::FLATPAK_REF_KIND_APP,
                                                                                          m_cancellable,
                                                                                          &error);
        for (uint i = 0; i < installed->len; i++) {
            FlatpakInstalledRef *ref = FLATPAK_INSTALLED_REF(g_ptr_array_index(installed, i));
            installed_map[QString::fromUtf8(flatpak_installed_ref_get_appdata_name(ref))] = true;
        }

        g_autoptr(GPtrArray) has_update = flatpak_installation_list_installed_refs_for_update(flatpakInstallation,
                                                                                               m_cancellable,
                                                                                               &error);
        for (uint i = 0; i < has_update->len; i++) {
            FlatpakInstalledRef *ref = FLATPAK_INSTALLED_REF(g_ptr_array_index(has_update, i));
            has_update_map[QString::fromUtf8(flatpak_installed_ref_get_appdata_name(ref))] = true;
        }

        GPtrArray* remotes = flatpak_installation_list_remotes(flatpakInstallation, m_cancellable, &error);
        if (remotes->len < 1) {
            std::cout << "no remotes" << std::endl;
            continue;
        }

        for (uint i = 0; i < remotes->len; i++) {
            FlatpakRemote *remote = FLATPAK_REMOTE(g_ptr_array_index(remotes, i));
            std::cout << flatpak_remote_get_name(remote) << std::endl;
//            g_autoptr(GFile) fileTimestamp = flatpak_remote_get_appstream_timestamp(remote, flatpak_get_default_arch());

//            g_autofree char *path_str = g_file_get_path(fileTimestamp);
//            QFileInfo fileInfo(QFile::encodeName(path_str));
            // Refresh appstream metadata in case they have never been refreshed or the cache is older than 6 hours
//            if (!fileInfo.exists() || fileInfo.lastModified().toUTC().secsTo(QDateTime::currentDateTimeUtc()) > 21600) {
//                refreshAppstreamMetadata(flatpakInstallation, remote);
//            } else {
//                integrateRemote(flatpakInstallation, remote);
//            }

            g_autoptr(GFile) appstreamDir = flatpak_remote_get_appstream_dir(remote, nullptr);
            if (!appstreamDir) {
                qWarning() << "No appstream dir for" << flatpak_remote_get_name(remote);
                continue;
            }
            g_autofree char *appstream_path_str = g_file_get_path(appstreamDir);
            auto appstreamDirPath = QString::fromUtf8(appstream_path_str);


            pool->clearMetadataLocations();
//            std::cout << appstreamDirPath.toStdString() << std::endl;
            pool->addMetadataLocation(appstreamDirPath);
            pool->setLocale(QLocale::system().name());
            pool->setFlags(AppStream::Pool::FlagReadCollection);
            pool->setCacheFlags(AppStream::Pool::CacheFlagUseUser);

            const QString subdir = flatpak_installation_get_id(flatpakInstallation) + QLatin1Char('/') + QString::fromUtf8(flatpak_remote_get_name(remote));
            pool->setCacheLocation(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/flatpak-appstream/" + subdir);
            QDir().mkpath(pool->cacheLocation());

            pool->load();

            for (AppStream::Component component : pool->componentsByKind(AppStream::Component::KindDesktopApp)) {
                if (!isAcceptableGame(&component)) {
                    continue;
                }
//                    std::cout << component.name().toStdString() << std::endl;
//                    FlatpakApp* app = new FlatpakApp(component, flatpakInstallation, remote);
//                    apps.append(app);
                FlatpakApp* app = new FlatpakApp(component, flatpakInstallation, remote);
//                Game* game = new Game(app, nullptr);
                app->m_installed = installed_map.contains(app->m_component.name());
                app->m_has_update = has_update_map.contains(app->m_component.name());
//                games.append(game);
                apps.append(app);
            }
        }
    }

    Q_EMIT loaded(apps);
}
/*
void FlatpakBackend::integrateRemote(FlatpakInstallation *flatpakInstallation, FlatpakRemote *remote)
{


    AppStream::Pool *pool = new AppStream::Pool(this);
    source->m_pool = pool;
    auto fw = new QFutureWatcher<bool>(this);
    const auto sourceName = source->name();
    connect(fw, &QFutureWatcher<bool>::finished, this, [this, fw, pool, source]() {
        if (fw->result()) {
            m_flatpakSources += source;
        } else {
            qWarning() << "Could not open the AppStream metadata pool" << pool->lastError();
        }
        metadataRefreshed();
        acquireFetching(false);
        fw->deleteLater();
    });
    acquireFetching(true);
    pool->clearMetadataLocations();
    pool->addMetadataLocation(appstreamDirPath);
    pool->setFlags(AppStream::Pool::FlagReadCollection);
    pool->setCacheFlags(AppStream::Pool::CacheFlagUseUser);

    const QString subdir = flatpak_installation_get_id(flatpakInstallation) + QLatin1Char('/') + sourceName;
    pool->setCacheLocation(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/flatpak-appstream/" + subdir);
    QDir().mkpath(pool->cacheLocation());
    fw->setFuture(QtConcurrent::run(&m_threadPool, pool, &AppStream::Pool::load));
}

*/
