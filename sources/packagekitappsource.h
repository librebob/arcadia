#ifndef PACKAGEKITAPPSOURCE_H
#define PACKAGEKITAPPSOURCE_H

#include <packagekitqt5/PackageKit/Daemon>
#include <packagekitqt5/PackageKit/Transaction>
#include <AppStreamQt/pool.h>
#include <QObject>
#include <QtCore>
#include <iostream>
#include "packagekitapp.h"
#include "game.h"
#include "appsource.h"

class PackageKitAppSource : public AppSource
{
public:
    explicit PackageKitAppSource(QObject *parent = nullptr);
    void loadGamesFromAppstreamData();
    AppStream::Pool* pool;
    QMap<QString, QString> m_app_ids;
    QMap<QString, bool> m_installed;
    PackageKit::Transaction* m_get_packages;

private:
    void doPool();

};

#endif // PACKAGEKITAPPSOURCE_H
