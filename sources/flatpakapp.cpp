#include "flatpakapp.h"

FlatpakApp::FlatpakApp(AppStream::Component &component, FlatpakInstallation *installation, FlatpakRemote *remote) : App()
{
    m_component = component;
    m_installation = installation;
    m_remote = remote;

    connect(&watcher, &QFutureWatcher<void>::started, this, &App::startProcessing);
    connect(&watcher, &QFutureWatcher<void>::finished, this, &App::stopProcessing);
}

QString FlatpakApp::id()
{
    return m_component.id().replace(".desktop", "");
}

QString FlatpakApp::sourceName()
{

    return QString::fromUtf8(flatpak_remote_get_name(m_remote)) +
            " (" + QString::fromUtf8(flatpak_installation_get_id(m_installation)) + ")";
}

bool FlatpakApp::installed()
{
    return m_installed;
}

bool FlatpakApp::hasUpdate()
{
    return m_has_update;
}

bool FlatpakApp::processing()
{
    return m_processing;
}

bool FlatpakApp::playing()
{
    return m_playing;
}

//void Game::updateFromRef(FlatpakRef *ref)
//{
//    setArch(QString::fromUtf8(flatpak_ref_get_arch(ref)));
//    setBranch(QString::fromUtf8(flatpak_ref_get_branch(ref)));
//    setCommit(QString::fromUtf8(flatpak_ref_get_commit(ref)));
//    setFlatpakName(QString::fromUtf8(flatpak_ref_get_name(ref)));
//    setType(flatpak_ref_get_kind(ref) == FLATPAK_REF_KIND_APP ? DesktopApp : extends().isEmpty() ? Runtime : Extension);
//    setObjectName(packageName());
//}

void FlatpakApp::updateFromAppStream()
{
    m_ref = m_component.bundle(AppStream::Bundle::KindFlatpak).id();
    g_autoptr(GError) localError = nullptr;
    g_autoptr(FlatpakRef) ref = flatpak_ref_parse(m_ref.toUtf8().constData(), &localError);
    if (!ref) {
        qDebug() << "failed to obtain ref" << m_ref << localError->message;
        return;
    }
//    updateFromRef(ref);
    m_arch = QString::fromUtf8(flatpak_ref_get_arch(ref));
    m_branch = QString::fromUtf8(flatpak_ref_get_branch(ref));
}


void FlatpakApp::play()
{
//    const QString desktopFile = installPath() + QLatin1String("/export/share/applications/") + appstreamId();
//    const QString runservice = QStringLiteral(CMAKE_INSTALL_FULL_LIBEXECDIR_KF5 "/discover/runservice");
//    if (QFile::exists(desktopFile) && QFile::exists(runservice)) {
//        QProcess::startDetached(runservice, {desktopFile});
//        return;
//    }
    updateFromAppStream();
    g_autoptr(GCancellable) cancellable = g_cancellable_new();
    g_autoptr(GError) localError = nullptr;

//    running_game = nullptr;

    if (!flatpak_installation_launch_full(m_installation,
                                     FlatpakLaunchFlags::FLATPAK_LAUNCH_FLAGS_NONE,
                                    id().toUtf8().constData(),
                                     m_arch.toUtf8().constData(),
                                     m_branch.toUtf8().constData(),
                                     nullptr,
                                     &running_game,
                                     cancellable,
                                     &localError)) {
        qWarning() << "Failed to launch " << id() << ": " << localError->message;
    } else {
        startPlaying();
        connect(&timer, &QTimer::timeout, this, [this](){
            if (!flatpak_instance_is_running(running_game)) {
                timer.stop();
                stopPlaying();
            }
        });
        timer.start(1000);
    }
}

FlatpakTransaction* FlatpakApp::getTransaction()
{
    g_autoptr(GCancellable) cancellable = g_cancellable_new();
    g_autoptr(GError) localError = nullptr;

    FlatpakTransaction* transaction = flatpak_transaction_new_for_installation(
                m_installation,
                cancellable,
                &localError);

    if (!transaction) {
        qWarning() << "Failed to get transaction: " << localError->message;
        return nullptr;
    }

    return transaction;
}

bool FlatpakApp::runTransaction(FlatpakTransaction* transaction)
{
    g_autoptr(GCancellable) cancellable = g_cancellable_new();
    g_autoptr(GError) localError = nullptr;

    if (!flatpak_transaction_run(transaction,
                            cancellable,
                            &localError)) {
        qWarning() << "Failed to run uninstall: " << localError->message;
        return false;
    }
    return true;
}

void FlatpakApp::install()
{
    if (m_installed) return;

    QFuture<void> future = QtConcurrent::run([this]() {
        updateFromAppStream();

        g_autoptr(GError) localError = nullptr;

        FlatpakTransaction* transaction = getTransaction();

        if(!flatpak_transaction_add_install(transaction,
                                            flatpak_remote_get_name(m_remote),
                                            m_ref.toUtf8().constData(),
                                            nullptr,
                                            &localError
                                            )){
            qWarning() << "Failed to add install: " << localError->message;
            return;
        }

        m_installed = runTransaction(transaction);
    });
    watcher.setFuture(future);
}

void FlatpakApp::uninstall()
{
    if (!m_installed) return;

    QFuture<void> future = QtConcurrent::run([this]() {
        updateFromAppStream();

        FlatpakTransaction* transaction = getTransaction();
        if (!transaction) {
            return;
        }

        g_autoptr(GError) localError = nullptr;

        if(!flatpak_transaction_add_uninstall(transaction,
                                            m_ref.toUtf8().constData(),
                                            &localError
                                            )){
            qWarning() << "Failed to add uninstall: " << localError->message;
            return;
        }

        m_installed = !runTransaction(transaction);
    });
    watcher.setFuture(future);
}
