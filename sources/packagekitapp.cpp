#include "packagekitapp.h"

PackageKitApp::PackageKitApp(AppStream::Component &component) : App()
{
    m_component = component;
}

QString PackageKitApp::id()
{
    return m_component.id().replace(".desktop", "");
}

QString PackageKitApp::sourceName()
{
    return PackageKit::Daemon::backendName();
}

QString PackageKitApp::packageId()
{
    int last_semi_colon = m_package_id.lastIndexOf(QLatin1Char(';'));

    if (m_installed && !m_package_id.contains("installed")) {
        auto res = m_package_id.insert(last_semi_colon+1, "installed:");
        return res;
    }

    if (!m_installed && m_package_id.contains("installed")) {
        auto res =  m_package_id.remove(last_semi_colon+1, 10);
        return res;
    }

    return m_package_id;
}

void PackageKitApp::play()
{
//    const QString launchable = m_component.launchable(AppStream::Launchable::KindDesktopId).entries().constFirst();
//    const QString service = QStandardPaths::locate(QStandardPaths::ApplicationsLocation, launchable);
//    qWarning() << launchable;
//    qWarning() << service;
    QProcess* p = new QProcess();
    connect(p, &QProcess::started, this, &App::startPlaying);
    connect(p, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, [this, p](int exitCode, QProcess::ExitStatus /*exitStatus*/) {
        stopPlaying();
        if (exitCode != 0) {
            qWarning() << "Failed to start " << id();
        }
        p->deleteLater();
    });
    p->start(id(), {}, QProcess::ReadOnly);
}

void PackageKitApp::install()
{
    startProcessing();

    m_install_package = PackageKit::Daemon::installPackage(packageId(), PackageKit::Transaction::TransactionFlagAllowReinstall);

    connect(m_install_package, &PackageKit::Transaction::errorCode, this, [](PackageKit::Transaction::Error error, const QString &details) {
        qWarning() << error << " " << details;
    });
    connect(m_install_package, &PackageKit::Transaction::finished, this, [this] (PackageKit::Transaction::Exit status, uint runtime) {
        if (status != PackageKit::Transaction::ExitSuccess) {
            qWarning() << "PackageKit Install Failed: " << status << " - Runtime: " << runtime;
        } else {
            this->m_installed = true;
        }
        stopProcessing();
    });
}

void PackageKitApp::uninstall()
{
    startProcessing();

    m_uninstall_package = PackageKit::Daemon::removePackage(packageId(), true, false, PackageKit::Transaction::TransactionFlagNone);

    connect(m_uninstall_package, &PackageKit::Transaction::errorCode, this, [](PackageKit::Transaction::Error error, const QString &details) {
        qWarning() << error << " " << details;
    });
    connect(m_uninstall_package, &PackageKit::Transaction::finished, this, [this] (PackageKit::Transaction::Exit status, uint runtime) {
        if (status != PackageKit::Transaction::ExitSuccess) {
            qWarning() << "PackageKit Uninstall Failed: " << status << " - Runtime: " << runtime;
        } else {
            this->m_installed = false;
        }
        stopProcessing();
    });
}
