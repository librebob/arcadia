#ifndef PACKAGEKITAPP_H
#define PACKAGEKITAPP_H

#include <QtCore>
#include <QObject>
#include <QStandardPaths>
#include <QProcess>
#include <AppStreamQt/component.h>
#include <packagekitqt5/PackageKit/Daemon>
#include <packagekitqt5/PackageKit/Transaction>
#include "app.h"

class PackageKitApp : public App
{
    Q_OBJECT
public:
    explicit PackageKitApp(AppStream::Component &component);
    QString id();
    QString sourceName();
    void play();
    void install();
    void uninstall();
    QString m_package_id;

private:
    PackageKit::Transaction* m_install_package;
    PackageKit::Transaction* m_uninstall_package;
    QString packageId();
Q_SIGNALS:

};

#endif // PACKAGEKITAPP_H
