#ifndef FLATPAKAPP_H
#define FLATPAKAPP_H

#include <AppStreamQt/component.h>
#include <QObject>
#include <QtConcurrent>
#include "flatpak-helper.h"
#include "app.h"

class FlatpakApp : public App
{
    Q_OBJECT
public:
    explicit FlatpakApp(AppStream::Component &component, FlatpakInstallation *installation, FlatpakRemote *remote);
    QString id();
    QString sourceName();
    bool installed();
    bool hasUpdate();
    bool processing();
    bool playing();
    void play();
    void install();
    void uninstall();

private:
    FlatpakInstallation* m_installation;
    FlatpakRemote* m_remote;

    QString m_ref;
    QString m_arch;
    QString m_branch;

    QTimer timer;
    QFutureWatcher<void> watcher;

    FlatpakInstance* running_game = nullptr;

    FlatpakTransaction* getTransaction();
    bool runTransaction(FlatpakTransaction*);
    void updateFromAppStream();

Q_SIGNALS:

};

#endif // FLATPAKAPP_H
