#ifndef FLATPAKAPPSOURCE_H
#define FLATPAKAPPSOURCE_H

#include "flatpak-helper.h"
#include "flatpakapp.h"
#include "appsource.h"
#include "game.h"
#include <AppStreamQt/pool.h>
#include <QObject>
#include <QtCore>
#include <iostream>

class FlatpakAppSource : public AppSource
{

public:
    explicit FlatpakAppSource(QObject *parent = nullptr);
    bool loadInstallations();
    void loadGamesFromAppstreamData();

private:
    GCancellable *m_cancellable;
    QVector<FlatpakInstallation *> m_installations;
    AppStream::Pool* pool;
    QList<QString> m_bad_licenses = QList<QString>({
        "LicenseRef-proprietary",
        "LicenseRef-Proprietary"
    });
};

#endif // FLATPAKAPPSOURCE_H
