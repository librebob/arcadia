#ifndef APPSOURCE_H
#define APPSOURCE_H

#include <QObject>
#include "app.h"

class AppSource : public QObject
{
    Q_OBJECT
public:
    AppSource(QObject *parent = nullptr);
    virtual void loadGamesFromAppstreamData() = 0;
    bool isAcceptableGame(AppStream::Component* component);

Q_SIGNALS:
    void loaded(QList<App*>);
};

#endif // APPSOURCE_H
