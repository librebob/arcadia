#ifndef APP_H
#define APP_H
#include <AppStreamQt/component.h>
#include <QObject>

class App : public QObject
{
    Q_OBJECT
public:
    AppStream::Component m_component;
    bool m_installed = false;
    bool m_has_update = false;
    bool m_playing = false;
    bool m_processing = false;

    virtual QString id() = 0;
    virtual QString sourceName() = 0;
    virtual void play() = 0;
    virtual void install() = 0;
    virtual void uninstall() = 0;

    void startProcessing();
    void stopProcessing();

    void startPlaying();
    void stopPlaying();

Q_SIGNALS:
    void stateChanged();
};

#endif // APP_H
