#include "packagekitappsource.h"

PackageKitAppSource::PackageKitAppSource(QObject *parent)
{
    pool = new AppStream::Pool;
    PackageKit::Daemon::global()->setHints(QStringList() << QStringLiteral("interactive=true")
                                                         << QStringLiteral("locale=%1").arg(qEnvironmentVariable("LANG")));

}

void PackageKitAppSource::loadGamesFromAppstreamData()
{


    m_get_packages = PackageKit::Daemon::getPackages();
    connect(m_get_packages, &PackageKit::Transaction::package, this, [this] (PackageKit::Transaction::Info info, const QString &packageID, const QString &summary) {
//        std::cout << PackageKit::Transaction::packageData(packageID).toStdString() << " " << packageID.toStdString() << std::endl;
        m_app_ids[PackageKit::Transaction::packageName(packageID).toLower()] = packageID;
        m_installed[PackageKit::Transaction::packageName(packageID).toLower()] = info == PackageKit::Transaction::InfoInstalled;
    });


    connect(m_get_packages, &PackageKit::Transaction::finished, this, &PackageKitAppSource::doPool);
}

void PackageKitAppSource::doPool()
{

    pool->setLocale(QLocale::system().name());
    pool->setFlags(AppStream::Pool::FlagReadCollection);
    pool->setCacheFlags(AppStream::Pool::CacheFlagUseUser);
    pool->setCacheLocation(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/packagekit-appstream");
    QDir().mkpath(pool->cacheLocation());
//    std::cout << pool->cacheLocation() << std::endl;
    pool->load();

    QList<App*> apps;
    for (AppStream::Component component : pool->components()) {
        if (!isAcceptableGame(&component)) {
            continue;
        }

        PackageKitApp* app = new PackageKitApp(component);

//        std::cout << app->id().toStdString()
//        << " "
//        << m_installed[app->id()]
//        << " "
//        << m_app_ids[app->id()].toStdString()
//        << std::endl;

        app->m_installed = m_installed[app->id().toLower()];
        app->m_package_id = m_app_ids[app->id().toLower()];
//        Game* game = new Game(app, nullptr);
        apps.append(app);
    }

//    PackageKit::Transaction *getID = PackageKit::Daemon::resolve("springlobby");
//    std::cout << "infoLES DO THIS" << std::endl;
//    QObject::connect(getID, &PackageKit::Transaction::package, this, [ = ] (PackageKit::Transaction::Info info, const QString &packageID) {
//        std::cout << info << std::endl;
//        std::cout << packageID.toStdString() << std::endl;
//    });

    Q_EMIT loaded(apps);
}
