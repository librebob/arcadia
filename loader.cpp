#include "loader.h"

Loader::Loader(QObject *parent) : QObject(parent)
{

}

void Loader::load()
{

//    QList<App*> flatpak_apps = m_flatpak_app_source.loadGamesFromAppstreamData();
//    QList<App*> package_kit_apps = m_package_kit_app_source.loadGamesFromAppstreamData();
    connect(&m_flatpak_app_source, &AppSource::loaded, this, &Loader::processApps);
    connect(&m_package_kit_app_source, &AppSource::loaded, this, &Loader::processApps);
//    QtConcurrent::run(&m_flatpak_app_source, &FlatpakAppSource::loadGamesFromAppstreamData);
//    QtConcurrent::run(&m_package_kit_app_source, &PackageKitAppSource::loadGamesFromAppstreamData);
    m_flatpak_app_source.loadGamesFromAppstreamData();
    m_package_kit_app_source.loadGamesFromAppstreamData();
}

void Loader::processApps(QList<App*> apps)
{
    for (auto app : apps) {
        apps_map.insert(app->id().toLower(), app);
    }

    m_wait_for--;

    if (m_wait_for != 0) {
        return;
    }

    for (QString app_id : apps_map.uniqueKeys()) {
        m_games.append(new Game(apps_map.values(app_id)));
    }

    Q_EMIT loaded(m_games);
        m_loading = false;
        Q_EMIT loading();
}

bool Loader::isLoading()
{
    return m_loading;
}
